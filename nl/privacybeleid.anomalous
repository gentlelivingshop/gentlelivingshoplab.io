<!DOCTYPE html>
<!--update language!-->
<html lang="nl">

<partial src="_head.html" />
<!--make changes here-->
<title>Our Privacy Policy | Gentle Living Shop</title>
<meta name="description" content="">
<!--open graph meta tags-->
<meta property="og:type" content="website">
<meta property="og:url" content="https://gentlelivingshop.org/nl/privacy-verklaring.html">
<meta property="og:title" content="Onze Privacy Verklaring | Gentle Living Shop">
<meta property="og:description" content="">
<meta property="og:image" content="#">
<!--this page in another language-->
<link rel="alternate" href="https://gentlelivingshop.org/privacy-policy.html" hreflang="en">
</head>

<body>

  <partial src="_header-nl.html" />

  <!--main content starts-->
  <main>
    <h1>Privacybeleid</h1>
    <hr>
    <details open class="section-box">
      <summary class="text-center">
        <h2>Wie zijn wij</h2>
      </summary>
      <p>Ons website adres is: <a href="./index.html" >https://gentlelivingshop.org</a> &#8211; een site van onze familie over dingen maken met de hand en een simpel leven leiden. We hebben deze website gemaakt en blijven deze ontwikkelen met een mentaliteit van vrije toegang en zijn daarbij zijn we ook bewust van privacy en veiligheid. Om deze website in de lucht te houden gebruiken we <a href="https://gitlab.com/">hosting diensten van GitLab</a>.</p>
    </details>

    <br>
    <details open class="section-box">
      <summary class="text-center">
        <h2>We traceren je niet (helemaal niet)</h2>
      </summary>
      <p><b>We verzamelen absoluut geen informatie over jou als je onze website bezoekt.</b> Je bent vrij om rond te kijken en we zullen je niet bespioneren. Er is geen analyse programme, geen cookies, geen IP adressen worden opgeslagen, je bent onzichtbaar voor ons.</p>
      <p>Hou er rekening mee dat we soms links delen naar andere websites waar je misschien wel getracked wordt (zoals YouTube). Dus voor extra duidelijkheid en om jouw privacy te beschermen, zijn de <b>links naar externe websites <u class="text-mediumblue cursor-pointer">medium blauwe gekleurd</u></b> en <b>links naar andere paginas binnen deze website <u class="cursor-pointer">zwart</u></b>.</p>
    </details>
    <br>
    <details class="section-box">
      <summary class="text-center">
        <h2>Jouw data is van jou</h2>
      </summary>
      <h3>Email</h3>
      <p> <b>Emails zijn alleen voor onze ogen en je bent vrij om jezelf te wissen uit ons digitale geheugen.</b> Als je ons een email stuurt is het versleuteld aan onze kant en beschermd met een sterk lokaal opgeslagen wachtwoord. We bevelen je aan om ook <a href="https://protonmail.com" target="_blank">Protonmail</a> te gebruiken om de hele keten te versleutelen. We bewaren je email adres niet buiten onze inbox en zullen alle email uitwisselingen verwijderen op jouw verzoek.</p>
      <h3>Chat</h3>
      Voor onze gemeenschappelijke chat groep gebruiken we het Matrix netwerk. Matrix is een geweldig gedecentraliseerd, open source communicatie protocol waarmee je berichten anoniem kan sturen. Het is een goed alternatief voor Whatsapp, Signal,
      Telegram etc. <b>Als je ons een chat stuurt door het Matrix netwerk is de hele keten versleuteld.</b> Onze Matrix groep is een gemeenschappelijke plek, iedereen die zich bij de groep inschrijft kan de berichten lezen. Je kan wel jouw berichten verwijderen van de groep wanneer je maar wilt.</p>
      <h3>Donaties</h3>
      <p>We accepteren donaties in Liberapay (open-source) en Ko-Fi. De betalingsgegevens die je gebruikt op deze platformen zullen worden gedeeld met de relevante betaalverwerkingsdienst, dat is of Stripe of Paypal. <b>Onze aanbeveling is om Liberapay te gebruiken en te betalen met Stripe.</b> Op deze manier ontvangen wij absoluut geen informatie over jou. Lees vooral ook het privacybeleid van <a href="https://liberapay.com/about/privacy" target="_blank">Liberapay</a> en <a href="https://more.ko-fi.com/privacy" target="_blank">Ko-Fi</a> voor meer informatie.</p>
    </details>
    <br>
    <details class="section-box">
      <summary class="text-center">
        <h2>Geen JavaScript*</h2>
      </summary>
      <p><b>We hebben alle code van deze website zelf geschreven exclusief met HTML en CSS.</b> Het is daarom een statische website wat betekent dat als je ons bezoekt, de computer de benodigde bestanden download zoals de code, fotos en lettertype van GitLab en onze
        interactie eindigt daar. Jouw browser doet lokaal de rest van het werk, het interpreteerd de bestanden en laat vervolgens de pagina zien. Tegenovergesteld van een dynamische website die de input van de bezoeker registreerd en de pagina daaraan aanpast.</p>
      <p>Een statische website zonder JavaScript is privacy vriendelijk en veilig omdat we geen programmas op jouw computer kunnen uitvoeren en ook ontvangen we geen informatie over jouw bewegingen op onze website. <b>Cookies en trackers zijn niet mogelijk.</b> <a href="./deze-website-maken.html">Lees meer over het maken van deze website</a></p>
      <h3>*Ingebedde websites</h3>
      <p>We hebben een <b>uitzondering</b> waarbij dynamische websites worden gebruikt en de code niet door ons geschreven is. We gebruiken de HTML functionilteit genaamd <code>iframe</code> om andere webpaginas binnen onze website te inbedden. Dit wordt gebruikt om je materiaal te laten zien wat anders moeilijk zou zijn om te delen.
        <!--<div class="iframe-container">  <iframe sandbox="allow-same-origin allow-scripts allow-popups" height=50px name="testiframe" src="./privacy-policy-iframe.html" class="responsive-iframe"></iframe></div>-->
      <p>Hieronder is een lijst van alle webpaginas die we op dit moment insluiten binnen onze website. We streven ernaar om onze website een veilige plek voor onze bezoekers te maken dus we inbedden alleen <b>open source paginas we tof vinden en veilig achten, allemaal zonder cookies en trackers</b>.</p>

      <ul>
        <li><a href="https://cryptpad.fr" target="testiframe"><b>CryptPad</b></a> wordt gebruikt om onze notities en code te laten zien waar we momenteel aan werken.</li>
        <li><a href="https://tube.privacytools.io/videos/embed/9c9de5e8-0a1e-484a-b099-e80766180a6d?subtitle=en&warningTitle=0" target="testiframe"><b>PeerTube</b></a> om onze videos te streamen die te groot zouden zijn om op GitLab te zetten.</li>
        <li><a href="https://flems.io/" target="testiframe"><b>Flems</b></a> om je interactieve code voorbeelden te laten zien, het stelt je in staat om te spelen met de code in real time.</li>
      </ul>

      <p>Als je meer privacy vriendelijke websites kent die we op deze manier kunnen gebruiken, laat het ons weten!</p>
    </details>
    <br>
    <details class="section-box">
      <summary class="text-center">
        <h2>Contact</h2>
      </summary>
      <p>Als je vragen of suggesties hebt, laat het ons weten via: <a href="mailto:gentlelivingshop@protonmail.com" target="_blank">gentlelivingshop at protonmail.com</a> of <a href="https://matrix.to/#/#gentlelivingshop:matrix.org?via=matrix.org"
          target="_blank">praat met ons op Matrix</a>.</p>
    </details>
    <br>
  </main>
  <!--main content ends-->

  <hr>

  <partial src="_footer-nl.html" />


</body>

</html>
